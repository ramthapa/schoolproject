from .models import *
from django import forms

class MessageForm(forms.ModelForm):
	class Meta:
		model = ContactForm
		fields = "__all__"
		widgets = { 
			'name':forms.TextInput(attrs={'placeholder':"enter your name..."}),
			'mobile':forms.NumberInput(attrs = {'placeholder':"enter your mobile number"}),
			'email':forms.EmailInput(attrs = {'placeholder':"enter your email"}),
			'subject':forms.TextInput(attrs = {'placeholder':'enter your subject'}),
			'message':forms.TextInput(attrs = {'placeholder':"write the messages"}),

		 }
