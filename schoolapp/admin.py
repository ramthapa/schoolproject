from django.contrib import admin
from .models import *

admin.site.register([Slider,Service,Testimonial,News,Category,Course,ContactForm])

# Register your models here.
