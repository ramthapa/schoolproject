from django.urls import path
from .views import *

app_name = 'schoolapp'

urlpatterns = [
   path('',ClientHomeView.as_view(),name = 'clienthome'),
   path('contact/',ClientContactView.as_view() , name = 'clientcontact'),
   path('about/',ClientAboutView.as_view(),name = 'clientabout'),

]