# Generated by Django 2.1.7 on 2019-04-01 07:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schoolapp', '0006_auto_20190401_1225'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContactForm',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('email', models.EmailField(max_length=254)),
                ('mobile', models.IntegerField(max_length=25)),
                ('subject', models.CharField(max_length=50)),
                ('message', models.TextField(max_length=30)),
            ],
        ),
    ]
