# Generated by Django 2.1.7 on 2019-03-31 07:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schoolapp', '0002_service_testimonial'),
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=40)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('content', models.TextField(max_length=100)),
                ('image', models.ImageField(upload_to='news')),
            ],
        ),
    ]
