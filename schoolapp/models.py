from django.db import models

# Create your models here.
class Slider(models.Model):
	title = models.CharField(max_length = 100)
	caption = models.CharField(max_length = 30)
	image = models.ImageField(upload_to = 'slider')
	link = models.CharField(max_length = 40)

	def __str__(self):
		return self.title

class Service(models.Model):
	title = models.CharField(max_length = 50)
	decription = models.TextField(max_length = 50)
	image = models.ImageField(upload_to = 'service')
	icon = models.CharField(max_length = 50)

	def __str__(self):
		return self.title

class Testimonial(models.Model):
	name = models.CharField(max_length = 20)
	image = models.ImageField(upload_to = 'testimonial')
	current_working = models.CharField(max_length = 50)
	saying = models.TextField(max_length = 100)

	def __str__(self):
		return self.name

class News(models.Model):
	title = models.CharField(max_length = 40)
	date = models.DateTimeField(auto_now_add = True)
	content = models.TextField(max_length = 100)
	image = models.ImageField(upload_to = 'news')

	def __str__(self):
		return self.title


class Category(models.Model):
	title = models.CharField(max_length = 30)
	icon = models.CharField(max_length = 13)
	image = models.ImageField(upload_to = 'course')

	def __str__(self):
		return self.title

class Course(models.Model):
	name = models.CharField(max_length = 50)
	category = models.ForeignKey(Category,on_delete = models.CASCADE , related_name = 'relatedcourse')
	content = models.TextField(max_length = 60)
	description = models.TextField(max_length = 50)
	icon = models.CharField(max_length = 50)
	image = models.ImageField(upload_to = 'course')
	duration = models.CharField(max_length = 50)

	def __str__(self):
		return self.name

class ContactForm(models.Model):
	name = models.CharField(max_length = 50)
	email = models.EmailField()
	mobile = models.CharField(max_length = 25)
	subject = models.CharField(max_length = 50)
	message = models.TextField(max_length = 30)

	def __str__(self):
		return self.name
