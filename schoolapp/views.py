from django.shortcuts import render
from django.views.generic import *
from .models import *
from .forms import *


# Create your views here.

class ClientHomeView(TemplateView):
	template_name = 'clienttemplates/clienthome.html'

	def get_context_data(self,**kwargs):
		context = super().get_context_data(**kwargs)
		context['sliders'] = Slider.objects.all()
		context['servicelist'] = Service.objects.all()
		context['alltestimonials'] = Testimonial.objects.all()
		context['allnews'] = News.objects.all()
		context['coursecategories'] = Category.objects.all()
		# context['courses'] = Course.objects.all()
		return context

class ClientContactView(CreateView):
	form_class = MessageForm
	template_name = 'clienttemplates/clientcontact.html'
	success_url = '/contact/'

class ClientAboutView(TemplateView):
	template_name = 'clienttemplates/clientabout.html'